//
// Created by Terez on 08.05.2020.
//

#ifndef PROJEKT_ZVIRE_H
#define PROJEKT_ZVIRE_H
#include "Zvire.h"


#include <iostream>

class Zvire {
protected:
    std::string m_jmeno;
    int m_hmotnost;
    int m_energie;
    int m_jidlo;
    int m_voda;
public:
    Zvire(std::string jmeno, int hmotnost, int energie, int jidlo, int voda);
    static Zvire* getZvire(std::string druh);
    std:: string getJmeno();
    virtual int getHmotnost()=0;
    virtual int getEnergie()=0;
    virtual int getJidlo()=0;
    virtual int getVoda()=0;
    virtual void snez()=0;
    virtual void umri()=0;
    virtual bool jeMrtve()=0;
    std::string getZnacka();
    virtual void printInfo()=0;
};


#endif //PROJEKT_ZVIRE_H
