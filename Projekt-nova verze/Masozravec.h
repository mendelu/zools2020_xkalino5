//
// Created by Terez on 08.05.2020.
//

#ifndef PROJEKT_MASOZRAVEC_H
#define PROJEKT_MASOZRAVEC_H
#include "Zvire.h"
#include <iostream>

class Masozravec : public Zvire {
    int m_sila;
public:
    Masozravec(std::string jmeno, int hmotnost, int energie, int jidlo, int voda, int sila);
    void sezerBylozravce();
    void sezerMasozravce();
    int getSila();
    int getHmotnost();
    int getEnergie();
    int getJidlo();
    int getVoda();
    void snez();
    void umri();
    bool jeMrtve();
    void printInfo();

};


#endif //PROJEKT_MASOZRAVEC_H
