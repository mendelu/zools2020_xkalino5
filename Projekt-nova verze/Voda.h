//
// Created by Terez on 08.05.2020.
//

#ifndef PROJEKT_VODA_H
#define PROJEKT_VODA_H

#include <iostream>
#include "Pole.h"


class Voda : public Pole {
public:
    Voda();

    int getPridejVodu();

    int getOdeberJidlo();

    int getOdeberEnergii();

    void pitVodu();

    std::string getZnacka();

};


#endif //PROJEKT_VODA_H
