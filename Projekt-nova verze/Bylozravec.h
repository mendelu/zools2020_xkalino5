//
// Created by Terez on 08.05.2020.
//

#ifndef PROJEKT_BYLOZRAVEC_H
#define PROJEKT_BYLOZRAVEC_H
#include <iostream>
#include "Zvire.h"
#include "Trava.h"



class Bylozravec : public Zvire {
    int m_rychlost;
public:
    Bylozravec(std::string jmeno, int hmotnost, int energie, int jidlo, int voda,int rychlost);

    void Utec();
    int getHmotnost();
    int getEnergie();
    int getJidlo();
    int getVoda();
    void snez();
    void umri();
    bool jeMrtve();
    void printInfo();
};

#endif //PROJEKT_BYLOZRAVEC_H
