//
// Created by Terez on 08.05.2020.
//

#ifndef PROJEKT_TRAVA_H
#define PROJEKT_TRAVA_H

#include "Pole.h"

class Trava : public Pole {
    int m_odeberEnergii;
    int m_pridejJidlo;
    int m_odeberVodu;

public:
    Trava();

    int getPridejJidlo();

    int getOdeberVodu();

    int getOdeberEnergii();

    std::string getZnacka();

};


#endif //PROJEKT_TRAVA_H
