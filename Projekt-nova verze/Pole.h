//
// Created by Terez on 08.05.2020.
//

#ifndef PROJEKT_POLE_H
#define PROJEKT_POLE_H

#include <iostream>
#include <vector>
#include <algorithm>
#include "Zvire.h"

class Pole {
    std::vector<Zvire *> m_zvirata;
    std::vector<Pole *> m_poleVOkoli;

    Pole *getNahodnePoleZOkoli();

public:
    void setPoleVOkoli(std::vector<Pole *> poleVOkoli);

    bool obsahujeZvire();

    void pridejZvire(Zvire *zvirata);

    virtual std::string getZnacka() = 0;

    void interakceZvirete();

    void posunZvirete(std::vector<Zvire *> *jizPosunutaZvirata);
};


#endif //PROJEKT_POLE_H
