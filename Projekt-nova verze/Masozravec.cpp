//
// Created by Terez on 08.05.2020.
//

#include "Masozravec.h"
Masozravec::Masozravec(std::string jmeno, int hmotnost, int energie, int jidlo, int voda, int sila): Zvire(jmeno,hmotnost,energie,jidlo,voda){
    m_sila = sila;
}
int Masozravec::getSila() {
    return m_sila;
}
void Masozravec::umri() {

}
bool Masozravec::jeMrtve() {
    return m_hmotnost <= 0;
}
int Masozravec::getVoda() {
    return m_voda;
}
int Masozravec::getJidlo() {
    return m_jidlo;
}
int Masozravec::getHmotnost() {
    return m_hmotnost;
}
void Masozravec::snez() {

}
int Masozravec::getEnergie() {
    return m_energie;
}
void Masozravec::printInfo() {

}
void Masozravec::sezerBylozravce() {

}
void Masozravec::sezerMasozravce() {

}

