//
// Created by Terez on 08.05.2020.
//

#include "Trava.h"

Trava::Trava() {
}

int Trava::getOdeberEnergii() {
    return m_odeberEnergii;
}

int Trava::getOdeberVodu() {
    return m_odeberVodu;
}

int Trava::getPridejJidlo() {
    return m_pridejJidlo;
}

std::string Trava::getZnacka() {
    return "T";
}