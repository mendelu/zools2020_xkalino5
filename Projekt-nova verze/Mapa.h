//
// Created by Terez on 08.05.2020.
//

#ifndef PROJEKT_MAPA_H
#define PROJEKT_MAPA_H

#include <array>
#include <vector>
#include <iostream>
#include "Pole.h"
#include "Voda.h"
#include "Pisek.h"
#include "Trava.h"

using std::cout;
using std::endl;

class Mapa {
private:
    // vytvorime hraci plochu (2d array) 5x5 policek
    std::array<std::array<Pole *, 5>, 5> m_mapa;
    // Využijeme NV singleton
    static Mapa *s_mapa; // tady si budeme uchovávat instanci
    // Konstruktor. zvenku nikdo nevytvori instanci pres new
    //Mapa();
    ~Mapa();

    bool jeValidniSouradnice(int x, int y);

    std::vector<Pole *> getPoleVOkoli(int x, int y);

public:
    Mapa();

    static Mapa *getMapa(); // vrat instanci
    void ulozNaPozici(int x, int y, Zvire *zvire);

    void interakceZvirat();

    void pohybZvirat();

    void vypisMapu();
};


#endif //PROJEKT_MAPA_H
