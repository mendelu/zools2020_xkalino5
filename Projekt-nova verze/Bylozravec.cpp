//
// Created by Terez on 08.05.2020.
//

#include "Bylozravec.h"



Bylozravec::Bylozravec(std::string jmeno, int hmotnost, int energie, int jidlo,
                       int voda, int rychlost):Zvire(jmeno,hmotnost,energie, jidlo, voda)
{
    m_rychlost=rychlost;
}
void Bylozravec::Utec() {

}
int Bylozravec::getEnergie() {
    return m_energie;
}

void Bylozravec::snez() {

}
int Bylozravec::getHmotnost() {
    return m_hmotnost;
}
int Bylozravec::getJidlo() {
    return m_jidlo;
}
int Bylozravec::getVoda() {
    return m_voda;
}
bool Bylozravec::jeMrtve() {
    return m_hmotnost <= 0;
}
void Bylozravec::umri() {

}
void Bylozravec::printInfo() {

}