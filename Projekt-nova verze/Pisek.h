//
// Created by Terez on 08.05.2020.
//

#ifndef PROJEKT_PISEK_H
#define PROJEKT_PISEK_H


#include <iostream>
#include "Pole.h"

class Pisek : public Pole {
    //Pisek();

public:
    Pisek();

    void getOdeberVodu();

    void getOdeberJidlo();

    void getDodejEnergii();

    std::string getZnacka();


};


#endif //PROJEKT_PISEK_H
