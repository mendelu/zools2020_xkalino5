//
// Created by Terez on 08.05.2020.
//

#include "Zvire.h"
#include "Masozravec.h"
#include "Bylozravec.h"

Zvire::Zvire(std::string jmeno, int hmotnost, int energie, int jidlo, int voda){
    m_jmeno=jmeno;
    m_hmotnost=hmotnost;
    m_energie=energie;
    m_jidlo=jidlo;
    m_voda=voda;

}


std::string Zvire::getZnacka() {
    // M oznacime mrtva zvirata
    if(jeMrtve()){
        return "M";
    }
        // Z oznacime ziva zvirata
    else{
        return  "Z";
    }
}
Zvire * Zvire::getZvire(std::string druh) {
    if(druh=="B"){
        return new Bylozravec("Panda",150,100,100,100,25);
    }
    else if (druh=="M") {
        return new Masozravec("Lev", 100, 100, 100, 100, 80);
    } else{
        std:: cerr << "Tento druh neexistuje" << std::endl;
        return nullptr;

    }
}