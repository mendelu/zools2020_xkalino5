//
// Pisek.h
// Created by Nikola Kalinova on 07.05.2020.
//

#ifndef PROJEKT_PISEK_H
#define PROJEKT_PISEK_H

#include <iostream>
#include "Pole.h"

class Pisek: public Pole {
Pisek();

public:
    void getOdeberVodu();
    void getOdeberJidlo();
    void getDodejEnergii();


};


#endif //PROJEKT_PISEK_H
