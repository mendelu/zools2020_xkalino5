//
// Zvire.h
// Created by Nikola Kalinova on 07.05.2020.
//

#ifndef PROJEKT_ZVIRE_H
#define PROJEKT_ZVIRE_H

#include <iostream>

class Zvire {
    std::string m_jmeno;
    int m_hmotnost;
    int m_energie;
    int m_jidlo;
    int m_voda;
    int m_rychlost;
public:
    Zvire(std::string jmeno, int hmotnost, int energie, int jidlo, int voda, int rychlost);
    ~Zvire();
    std:: string getJmeno();
    int getHmotnost();
    int getEnergie();
    int getJidlo();
    int getVoda();
    int getRychlost();
    virtual void snez()=0;
    void umri();
    bool jeMrtve();
    std::string getZnacka();
};


#endif //PROJEKT_ZVIRE_H
