//
// Zvire.cpp
// Created by Nikola Kalinova on 07.05.2020.
//

#include "Zvire.h"

Zvire::Zvire(std::string jmeno, int hmotnost, int energie, int jidlo, int voda, int rychlost){
    m_jmeno=jmeno;
    m_hmotnost=hmotnost;
    m_energie=energie;
    m_jidlo=jidlo;
    m_voda=voda;
    m_rychlost=rychlost;
}
std::string Zvire::getJmeno(){
    return m_jmeno;
}
int Zvire::getHmotnost() {
    return m_hmotnost;
}
int Zvire::getEnergie() {
    return m_energie;
}
int Zvire::getJidlo()  {
    return m_jidlo;
}
int Zvire::getVoda() {
    return m_voda;
}
int Zvire::getRychlost() {
    return m_rychlost;
}
void Zvire::snez() {

}
void Zvire::umri() {

}
bool Zvire::jeMrtve() {
    return m_hmotnost <= 0;
}
std::string Zvire::getZnacka() {
    // M oznacime mrtva zvirata
    if(jeMrtve()){
        return "M";
    }
        // Z oznacime ziva zvirata
    else{
        return  "Z";
    }
}