//
// Created by kikak on 17.05.2020.
//

#ifndef ZKOUSKA_LETISTE_H
#define ZKOUSKA_LETISTE_H
#include <iostream>
#include "Letadlo.h"
#include <vector>

 class Letiste {
    std::vector<Letadlo*>m_letadla;
    std::string m_nazev;
    int m_kapacita;
    Letadlo*letadlo;
 public:
    Letiste(std::string nazev,int kapacita);
    void pridejLetadlo(Letadlo*letadlo);
    void odeberLetadlo(int odkud);
    void printInfo();
};


#endif //ZKOUSKA_LETISTE_H
