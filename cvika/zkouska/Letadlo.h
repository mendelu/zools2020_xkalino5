//
// Created by kikak on 17.05.2020.
//

#ifndef ZKOUSKA_LETADLO_H
#define ZKOUSKA_LETADLO_H
#include <iostream>

class Letadlo {
protected:
    int m_spotreba;
    std::string m_oznaceni;
public:
    Letadlo(int spotreba,std::string oznaceni);
   virtual int getSpotreba()=0;
   virtual void printInfo()=0;


};


#endif //ZKOUSKA_LETADLO_H
