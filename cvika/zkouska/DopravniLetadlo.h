//
// Created by kikak on 17.05.2020.
//

#ifndef ZKOUSKA_DOPRAVNILETADLO_H
#define ZKOUSKA_DOPRAVNILETADLO_H
#include <iostream>
#include "Letadlo.h"

class DopravniLetadlo: public Letadlo {
    int m_pocetPasazeru;
public:
    DopravniLetadlo(int spotreba,std::string oznaceni,int pocetPasazeru);
    int getSpotreba();
    void printInfo();

};


#endif //ZKOUSKA_DOPRAVNILETADLO_H
