cmake_minimum_required(VERSION 3.16)
project(zkouska)

set(CMAKE_CXX_STANDARD 14)

add_executable(zkouska main.cpp Letiste.cpp Letiste.h Letadlo.cpp Letadlo.h DopravniLetadlo.cpp DopravniLetadlo.h NakladniLetadlo.cpp NakladniLetadlo.h)