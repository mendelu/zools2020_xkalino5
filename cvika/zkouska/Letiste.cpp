//
// Created by kikak on 17.05.2020.
//

#include "Letiste.h"

Letiste::Letiste(std::string nazev, int kapacita) {
    m_nazev=nazev;
    m_kapacita=kapacita;
    for(Letadlo* &letadlo: m_letadla){
        letadlo= nullptr;
    }
}
void Letiste::pridejLetadlo(Letadlo* letadlo) {
    if (m_letadla.size()<m_kapacita) {
        m_letadla.push_back(letadlo);
    }else
    {
        std::cout<<"Kapacita letiste je plna:"<<std::endl;
    }
}
void Letiste::odeberLetadlo(int odkud) {
    m_letadla.at(odkud)= nullptr;
}
void Letiste::printInfo() {
    std::cout<<"Nazev letiste: "<<m_nazev<<std::endl;
    std::cout<<"Kapacita: "<<m_kapacita<<std::endl;
    for(Letadlo* letadlo:m_letadla ){
        if(letadlo!= nullptr){
            letadlo->printInfo();
        }
    }
}