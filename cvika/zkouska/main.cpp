#include <iostream>
#include "Letiste.h"
#include "NakladniLetadlo.h"
#include "DopravniLetadlo.h"

int main() {
    Letiste* l1= new Letiste("Praha",2);
    NakladniLetadlo* n1= new NakladniLetadlo(50,"n1",190);
    NakladniLetadlo* n2= new NakladniLetadlo(60,"n2",150);
    l1->pridejLetadlo(n1);
    l1->pridejLetadlo(n2);
    l1->printInfo();
    l1->odeberLetadlo(0);
    l1->printInfo();
    return 0;
}
