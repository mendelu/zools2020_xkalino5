//
// Created by kikak on 17.05.2020.
//

#include "NakladniLetadlo.h"
NakladniLetadlo::NakladniLetadlo(int spotreba, std::string oznaceni, int vahaZavazadla) :Letadlo(spotreba,oznaceni){
    m_vahaZavazadla=vahaZavazadla;
}
int NakladniLetadlo::getSpotreba() {
    return m_spotreba*(m_vahaZavazadla/30);
}
void NakladniLetadlo::printInfo() {
    std::cout<<"Oznaceni: "<<m_oznaceni<<std::endl;
    std::cout<<"Vaha: "<<m_vahaZavazadla<<std::endl;
    std::cout<<"Spotreba: "<<getSpotreba()<<std::endl;
}