//
// Created by kikak on 17.05.2020.
//

#ifndef ZKOUSKA_NAKLADNILETADLO_H
#define ZKOUSKA_NAKLADNILETADLO_H

#include <iostream>
#include "Letadlo.h"
class NakladniLetadlo: public Letadlo{
    int m_vahaZavazadla;
public:
    NakladniLetadlo(int spotreba, std::string oznaceni, int vahaZavazadla);
    int getSpotreba();
    void printInfo();

};


#endif //ZKOUSKA_NAKLADNILETADLO_H
