//
// Created by kikak on 17.05.2020.
//

#include "DopravniLetadlo.h"

 DopravniLetadlo::DopravniLetadlo(int spotreba, std::string oznaceni, int pocetPasazeru):Letadlo(spotreba,oznaceni)
 {
    m_pocetPasazeru=pocetPasazeru;
}
int DopravniLetadlo::getSpotreba() {
    return (m_pocetPasazeru*90)/m_spotreba;
}
void DopravniLetadlo::printInfo() {
    std::cout<<"Oznaceni: "<<m_oznaceni<<std::endl;
    std::cout<<"Pocet pasazeru: "<<m_pocetPasazeru<<std::endl;
    std::cout<<"Spotreba: "<<getSpotreba()<<std::endl;
}