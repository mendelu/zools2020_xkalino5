//
// Created by kikak on 14.05.2020.
//

#ifndef ZAPOCET_KNIHA_H
#define ZAPOCET_KNIHA_H
#include "Publikace.h"
#include <iostream>

class Kniha: public Publikace {
    std::string m_autor;
    int m_ISBN;
public:
    Kniha(std::string nazev,std::string datum,std::string autor,int ISBN);
   void printInfo();

};


#endif //ZAPOCET_KNIHA_H
