//
// Created by kikak on 14.05.2020.
//

#include "Casopis.h"
Casopis::Casopis(std::string nazev, std::string datum, int rocnik, int ISSN):Publikace(nazev,datum) {
    m_rocnik=rocnik;
    m_ISSN=ISSN;
}
void Casopis::printInfo() {
    std::cout<<"Nazev: "<<m_nazev<<std::endl;
    std::cout<<"Datum: "<<m_datumVydani<<std::endl;
    std::cout<<"Rocnik: "<<m_rocnik<<std::endl;
    std::cout<<"ISSN: "<<m_ISSN<<std::endl;
}