//
// Created by kikak on 14.05.2020.
//

#include "Kniha.h"
Kniha::Kniha(std::string nazev, std::string datum, std::string autor, int ISBN):Publikace(nazev,datum) {
    m_autor=autor;
    m_ISBN=ISBN;
}
void Kniha::printInfo() {
    std::cout<<"Nazev: "<<m_nazev<<std::endl;
    std::cout<<"Datum: "<<m_datumVydani<<std::endl;
    std::cout<<"Autor: "<<m_autor<<std::endl;
    std::cout<<"ISBN: "<<m_ISBN<<std::endl;
}