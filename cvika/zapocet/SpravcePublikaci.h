//
// Created by kikak on 14.05.2020.
//

#ifndef ZAPOCET_SPRAVCEPUBLIKACI_H
#define ZAPOCET_SPRAVCEPUBLIKACI_H
#include <iostream>
#include <vector>
#include "Publikace.h"
#include "Casopis.h"
#include "Kniha.h"

class SpravcePublikaci {
std::vector<Publikace*>m_publikace;
public:
    void pridejPublikaci(Publikace *publikace);
    Casopis* vytvorCasopis(std::string nazev,std::string datum,int rocnik,int ISSN);    //Datum bych dal spíš jako string než char
    Kniha* vytvorKnihu(std::string nazev,std::string datum,std::string autor,int ISBN);//Datum bych dal spíš jako string než char
    void vypisPublikace();
    void vypisPublikaci(std::string nazev);
    void prejmenujPublikaci(std::string stareJmeno, std::string noveJmeno);
};


#endif //ZAPOCET_SPRAVCEPUBLIKACI_H
