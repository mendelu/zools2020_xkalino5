//
// Created by kikak on 14.05.2020.
//

#include "Publikace.h"
Publikace::Publikace(std::string nazev, std::string datum) {
    m_nazev=nazev;
    m_datumVydani=datum;
}
std::string Publikace::getNazev() {
    return m_nazev;
}
void Publikace::setNazev(std::string novyNazev) {
    m_nazev=novyNazev;
}
