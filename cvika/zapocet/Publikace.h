//
// Created by kikak on 14.05.2020.
//

#ifndef ZAPOCET_PUBLIKACE_H
#define ZAPOCET_PUBLIKACE_H

#include <iostream>
class Publikace {
protected:
    std::string m_nazev;
    std::string  m_datumVydani;
public:
    Publikace(std::string nazev,std::string datum);
    virtual void printInfo()=0;
    std::string getNazev();
    void setNazev(std::string novyNazev);


};


#endif //ZAPOCET_PUBLIKACE_H
