//
// Created by kikak on 14.05.2020.
//

#ifndef ZAPOCET_CASOPIS_H
#define ZAPOCET_CASOPIS_H

#include <iostream>
#include "Publikace.h"
class Casopis: public Publikace {
    int m_rocnik;
    int m_ISSN;
public:
    Casopis(std::string nazev,std::string datum,int rocnik,int ISSN);
    void printInfo();


};


#endif //ZAPOCET_CASOPIS_H
