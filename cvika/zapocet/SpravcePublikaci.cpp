//
// Created by kikak on 14.05.2020.
//

#include "SpravcePublikaci.h"

void SpravcePublikaci::prejmenujPublikaci(std::string stareJmeno, std::string noveJmeno) {
    for (int i = 0; i < m_publikace.size(); i++)
    {
        if(m_publikace.at(i)->getNazev()==stareJmeno)
        {
            m_publikace.at(i)->setNazev(noveJmeno);
        }
    }
}

void SpravcePublikaci::vypisPublikace() {
    for (int i = 0; i < m_publikace.size(); i++)
    {
        m_publikace.at(i)->printInfo(); //Nebo jen std::cout<<m_publikace.at(i)->getNazev();
    }
}
void SpravcePublikaci::vypisPublikaci(std::string nazev) {
    for (int i = 0; i < m_publikace.size(); i++)
    {
        if(m_publikace.at(i)->getNazev()==nazev)
        {
            m_publikace.at(i)->printInfo();
        }
    }
}

void SpravcePublikaci::pridejPublikaci(Publikace *publikace) {
    m_publikace.push_back(publikace);
}
Kniha* SpravcePublikaci::vytvorKnihu(std::string nazev, std::string datum, std::string autor, int ISBN) {
    Kniha *k = new Kniha(nazev,datum,autor,ISBN);
    m_publikace.push_back(k);
    return k;
}
Casopis* SpravcePublikaci::vytvorCasopis(std::string nazev,std::string datum, int rocnik, int ISSN) {
    Casopis *c = new Casopis(nazev,datum,rocnik,ISSN);
    m_publikace.push_back(c);
    return c;
}