#include <iostream>
#include "SpravcePublikaci.h"
#include "Publikace.h"


int main() {
    SpravcePublikaci* s1= new SpravcePublikaci();
   s1->vytvorKnihu("Kniha","12.3.","ja",2435);
   s1->vytvorCasopis("Casopis","3.5.",1998,901);
   s1->vypisPublikace();
   s1->prejmenujPublikaci("Kniha","NovaKniha");
   s1->vypisPublikaci("NovaKniha");
   delete s1;
    return 0;
}
