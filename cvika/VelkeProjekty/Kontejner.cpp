//
// Created by kikak on 12.05.2020.
//

#include "Kontejner.h"

Kontejner::Kontejner(int vaha, std::string obsah, std::string jmenoMajitele) {
    m_vaha=vaha;
    m_obsah=obsah;
    m_jmenoMajitele=jmenoMajitele;
}
int Kontejner::getVaha() {
    return m_vaha;
}

std::string Kontejner::getObsah() {
    return m_obsah;
}
std::string Kontejner::getMajitel() {
    return m_jmenoMajitele;
}
void Kontejner::printInfo() {
    std::cout<<"Vaha kontejneru je: "<<m_vaha<<std::endl;
    std::cout<<"Obsah kontejneru je: "<<m_obsah<<std::endl;
    std::cout<<"Jmeno majitele kontejneru je: "<<m_jmenoMajitele<<std::endl;
}