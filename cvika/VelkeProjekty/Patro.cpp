//
// Created by kikak on 12.05.2020.
//

#include "Patro.h"

Patro::Patro(std::string jmeno) {
    m_jmeno=jmeno;
    for (Kontejner* &kontejnery:m_kontejnery){
        kontejnery= nullptr;
    }
}
void Patro::pridejKontejner(int kam,Kontejner*kontejner) {
        m_kontejnery[kam]=kontejner;
}

void Patro::odeberKontejner(int odkud) {
    m_kontejnery[odkud]= nullptr;
}
void Patro::printInfo() {
    for(Kontejner*kontejner:m_kontejnery)
    { if(kontejner!= nullptr){
        kontejner->printInfo();
    }

    }
}