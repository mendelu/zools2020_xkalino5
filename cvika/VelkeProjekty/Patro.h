//
// Created by kikak on 12.05.2020.
//

#ifndef VELKEPROJEKTY_PATRO_H
#define VELKEPROJEKTY_PATRO_H

#include <iostream>
#include <array>
#include "Kontejner.h"
class Patro {
    std::array<Kontejner*,10>m_kontejnery;
    std::string m_jmeno;
public:
    Patro(std::string jmeno);
    void pridejKontejner(int kam,Kontejner*kontejner);
    void odeberKontejner(int odkud);
    void printInfo();

};


#endif //VELKEPROJEKTY_PATRO_H
