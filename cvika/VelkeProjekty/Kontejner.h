//
// Created by kikak on 12.05.2020.
//

#ifndef VELKEPROJEKTY_KONTEJNER_H
#define VELKEPROJEKTY_KONTEJNER_H
#include <iostream>

class Kontejner {
private:
    int m_vaha;
    std::string m_obsah;
    std::string m_jmenoMajitele;
public:
    Kontejner(int vaha,std::string obsah, std::string jmenoMajitele);
    int getVaha();
    std::string getObsah();
    std::string getMajitel();
    void  printInfo();
};


#endif //VELKEPROJEKTY_KONTEJNER_H
