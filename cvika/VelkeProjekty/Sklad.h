//
// Created by kikak on 12.05.2020.
//

#ifndef VELKEPROJEKTY_SKLAD_H
#define VELKEPROJEKTY_SKLAD_H

#include "Patro.h"
#include <iostream>
#include <array>
class Sklad {
    std::array<Patro*,5>m_patro;
public:
    Sklad();
    ~Sklad();
    void ulozDoSkladu(int patro,int pozice,Kontejner*kontejner);
    void odeberZeSkladu(int patro,int pozice);
    void printInfo();

};


#endif //VELKEPROJEKTY_SKLAD_H
