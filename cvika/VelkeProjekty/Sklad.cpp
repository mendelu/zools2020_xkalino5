//
// Created by kikak on 12.05.2020.
//

#include "Sklad.h"

Sklad::Sklad() {

    for(Patro* &patro : m_patro){
        patro = new Patro("Patro");
    }
}

Sklad::~Sklad() {
    for (Patro* &patro:m_patro){
        delete patro;

    }
}
void Sklad::ulozDoSkladu(int patro, int pozice, Kontejner *kontejner) {
    m_patro.at(patro)->pridejKontejner(pozice,kontejner);
}
void Sklad::odeberZeSkladu(int patro, int pozice) {
    m_patro.at(patro)->odeberKontejner(pozice);
}
void Sklad::printInfo() {
    for (Patro*patro:m_patro){
        if (patro!= nullptr){
            patro->printInfo();
        }
    }
}