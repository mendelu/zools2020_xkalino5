//
// Created by kikak on 17.05.2020.
//

#ifndef TOVARNA_PRVNITOVARNA_H
#define TOVARNA_PRVNITOVARNA_H

#include "Tovarna.h"
#include "Intel.h"
#include "SSDdisk.h"
class PrvniTovarna: public Tovarna {
public:
    Disk * getDisk();
    Procesor* getProcesor();

};


#endif //TOVARNA_PRVNITOVARNA_H
