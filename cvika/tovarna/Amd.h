//
// Created by kikak on 17.05.2020.
//

#ifndef TOVARNA_AMD_H
#define TOVARNA_AMD_H
#include <iostream>
#include "Procesor.h"
class Amd : public Procesor{
    int m_frekvence;
    int m_velikostCache;
public:
    Amd(int frekvence,int velikost);
    int getVykon();

};


#endif //TOVARNA_AMD_H
