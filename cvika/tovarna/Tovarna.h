//
// Created by kikak on 17.05.2020.
//

#ifndef TOVARNA_TOVARNA_H
#define TOVARNA_TOVARNA_H

#include "Disk.h"
#include "Procesor.h"
class Tovarna {
public:
    virtual Disk* getDisk() = 0;
    virtual Procesor* getProcesor() = 0;

};


#endif //TOVARNA_TOVARNA_H
