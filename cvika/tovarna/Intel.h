//
// Created by kikak on 17.05.2020.
//

#ifndef TOVARNA_INTEL_H
#define TOVARNA_INTEL_H

#include <iostream>
#include "Procesor.h"
class Intel: public Procesor {

    int m_frekvence;
public:
    Intel(int frekvence);
    int getVykon();

};


#endif //TOVARNA_INTEL_H
