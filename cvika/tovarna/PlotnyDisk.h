//
// Created by kikak on 17.05.2020.
//

#ifndef TOVARNA_PLOTNYDISK_H
#define TOVARNA_PLOTNYDISK_H
#include "Disk.h"
#include <iostream>
class PlotnyDisk: public Disk {
    int m_rychlostZapisu;
public:
    PlotnyDisk(int  rychlostZapisu);
    int getRychlost();

};


#endif //TOVARNA_PLOTNYDISK_H
