//
// Created by kikak on 17.05.2020.
//

#ifndef TOVARNA_SSDDISK_H
#define TOVARNA_SSDDISK_H

#include <iostream>
#include "Disk.h"
class SSDdisk: public Disk{
    int m_rychlostZapisu;
    int m_hloubkaZapisu;
public:
    SSDdisk(int rychlostZapisu,int hloubkaZapisu);
    int getRychlost();


};


#endif //TOVARNA_SSDDISK_H
