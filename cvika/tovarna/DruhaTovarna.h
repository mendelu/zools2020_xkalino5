//
// Created by kikak on 17.05.2020.
//

#ifndef TOVARNA_DRUHATOVARNA_H
#define TOVARNA_DRUHATOVARNA_H

#include "Tovarna.h"
#include "Amd.h"
#include "PlotnyDisk.h"
class DruhaTovarna: public Tovarna {
public:
    Disk * getDisk();
    Procesor* getProcesor();

};


#endif //TOVARNA_DRUHATOVARNA_H
