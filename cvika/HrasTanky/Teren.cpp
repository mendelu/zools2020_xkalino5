//
// Created by kikak on 16.05.2020.
//

#include "Teren.h"
Teren::Teren(int skon) {
    m_skon=skon;
    m_tank= nullptr;
}
Tank* Teren::getTank() {
    return m_tank;
}

void Teren::setTank(Tank *tank) {
    m_tank = tank;
}

bool Teren::obsahujeTank() {
    return m_tank != nullptr;
}
