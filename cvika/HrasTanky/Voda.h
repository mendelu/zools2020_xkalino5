//
// Created by kikak on 16.05.2020.
//

#ifndef HRASTANKY_VODA_H
#define HRASTANKY_VODA_H
#include "Teren.h"
#include <iostream>

class Voda:public Teren {
    int m_hloubka;
public:
    Voda(int skon,int hloubka);
    int getBonus();
    void printInfo();
    std::string getZnacka();

};


#endif //HRASTANKY_VODA_H
