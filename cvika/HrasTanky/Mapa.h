//
// Created by kikak on 16.05.2020.
//

#ifndef HRASTANKY_MAPA_H
#define HRASTANKY_MAPA_H
#include <iostream>
#include <array>
#include "Teren.h"
#include "Les.h"
#include "Voda.h"

class Mapa {
    std::array<std::array<Teren*,5>,5>m_mapa;
public:
Mapa();

void Vykresli();
void printInfo();
    void umistiTank(int x, int y, Tank *tank);
    void utok(int s_x, int s_y, int d_x, int d_y);


};


#endif //HRASTANKY_MAPA_H
