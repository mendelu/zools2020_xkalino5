//
// Created by kikak on 16.05.2020.
//

#ifndef HRASTANKY_LES_H
#define HRASTANKY_LES_H

#include <iostream>
#include "Teren.h"

class Les:public Teren {
    int m_propustnost;
public:
    Les(int skon,int propustnost);
    int getBonus();
    void printInfo();
    std::string getZnacka();

};


#endif //HRASTANKY_LES_H
