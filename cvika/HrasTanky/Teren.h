//
// Created by kikak on 16.05.2020.
//

#ifndef HRASTANKY_TEREN_H
#define HRASTANKY_TEREN_H
#include <iostream>
#include "Tank.h"
class Teren {
protected:
    int m_skon;
    Tank* m_tank;
public:
    Teren(int skon);
    void setTank(Tank* tank);
    Tank* getTank();
    bool obsahujeTank();
    virtual int getBonus()=0;
    virtual void printInfo()=0;
    virtual std::string getZnacka()=0;

};


#endif //HRASTANKY_TEREN_H
