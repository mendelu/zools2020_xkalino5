//
// Created by kikak on 16.05.2020.
//

#ifndef HRASTANKY_TANK_H
#define HRASTANKY_TANK_H

#include <iostream>
class Tank {
    std::string m_jmeno;
    int m_zivoty;
    int m_sila_utoku;
public:
    Tank(std::string jmeno, int sila);
    void zautoc(Tank* tank, int bonus_terenu);
    void snizZivoty(int kolik);
    void printInfo();

};


#endif //HRASTANKY_TANK_H
