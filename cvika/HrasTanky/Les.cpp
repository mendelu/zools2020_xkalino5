//
// Created by kikak on 16.05.2020.
//

#include "Les.h"
Les::Les(int skon, int propustnost) :Teren(skon)
{
    m_propustnost=propustnost;
}
int Les::getBonus() {
    return m_skon * 10 - m_propustnost;
}

void Les::printInfo() {
    std::cout << "Les: " << getBonus() << std::endl;
}

std::string Les::getZnacka() {
    return "L";
}