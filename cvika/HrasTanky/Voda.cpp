//
// Created by kikak on 16.05.2020.
//

#include "Voda.h"
Voda::Voda(int skon, int hloubka):Teren(skon) {
    m_hloubka=hloubka;
}
int Voda::getBonus() {
    return m_hloubka-5-m_skon;
}
void Voda::printInfo() {
    std::cout<<"Voda:"<<getBonus()<<std::endl;
}
std::string Voda::getZnacka() {
    return "V";
}