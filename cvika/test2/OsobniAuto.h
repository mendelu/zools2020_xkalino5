//
// Created by kikak on 13.05.2020.
//

#ifndef TEST2_OSOBNIAUTO_H
#define TEST2_OSOBNIAUTO_H
#include <iostream>
#include "Automobil.h"

class OsobniAuto:public Automobil{
    bool m_bourano;
public:
    OsobniAuto(std::string model,int najetoKm, int puvodniCena,bool bourano);
    int getCena();
    void printInfo();

};


#endif //TEST2_OSOBNIAUTO_H
