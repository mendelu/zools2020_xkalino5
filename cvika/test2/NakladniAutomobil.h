//
// Created by kikak on 13.05.2020.
//

#ifndef TEST2_NAKLADNIAUTOMOBIL_H
#define TEST2_NAKLADNIAUTOMOBIL_H
#include <iostream>
#include "Automobil.h"

class NakladniAutomobil: public Automobil {
    int m_nosnost;
public:
    NakladniAutomobil(std::string model,int najetoKm, int puvodniCena,int nosnost);
    int getCena();
    void printInfo();

};


#endif //TEST2_NAKLADNIAUTOMOBIL_H
