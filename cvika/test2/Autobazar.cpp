//
// Created by kikak on 13.05.2020.
//

#include "Autobazar.h"
Autobazar::Autobazar() {
for (Automobil*&automobil:m_auto){
    automobil= nullptr;
}
}
int Autobazar::getMajetek(){
int soucetDohromady=0;
    for(int i=0; i< m_auto.size();i++){
        soucetDohromady+= m_auto.at(i)->getCena();
    }
    return soucetDohromady;
}
  void Autobazar::pridejAutomobil(Automobil *automobil) {
    m_auto.push_back(automobil);
}
 int Autobazar::stocKm(int kolikStocit, Automobil *automobil) {
   return automobil->getNajetoKm()-kolikStocit;
}
void Autobazar::vypisAuta() {
    for (int i = 0; i < m_auto.size(); ++i) {
         m_auto.at(i)->printInfo();
    }
}
