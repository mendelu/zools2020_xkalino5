//
// Created by kikak on 13.05.2020.
//

#ifndef TEST2_AUTOMOBIL_H
#define TEST2_AUTOMOBIL_H
#include <iostream>

class Automobil {
protected:
    std::string m_modelAuta;
    int m_najetoKm;
    int m_puvodniCena;
public:
    Automobil(std::string model,int najetoKm,int puvodniCena);
    std::string getModel();
    int getNajetoKm();
    int getPuvodniCena();
    virtual int getCena()=0;
    virtual void printInfo()=0;


};


#endif //TEST2_AUTOMOBIL_H
