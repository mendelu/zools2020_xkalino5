//
// Created by kikak on 13.05.2020.
//

#include "Automobil.h"


Automobil::Automobil(std::string model, int najetoKm, int puvodniCena) {
    m_modelAuta=model;
    m_najetoKm=najetoKm;
    m_puvodniCena=puvodniCena;
}
std::string Automobil::getModel() {
    return m_modelAuta;
}
int Automobil::getNajetoKm() {
    return  m_najetoKm;
}
int Automobil::getPuvodniCena() {
    return m_puvodniCena;
}
