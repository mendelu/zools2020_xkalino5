//
// Created by kikak on 13.05.2020.
//

#include "OsobniAuto.h"


OsobniAuto::OsobniAuto(std::string model, int najetoKm, int puvodniCena, bool bourano):
Automobil(model,najetoKm,puvodniCena)
{
    m_bourano=bourano;
}
int OsobniAuto::getCena() {

        return (1-(m_najetoKm/300000)) * m_puvodniCena * (1-0.5*m_bourano);

}
void OsobniAuto::printInfo() {
    std::cout<<"Model: "<<m_modelAuta<<std::endl;
    std::cout<<"Najeto: "<<m_najetoKm<<std::endl;
    std::cout<<"Puvodni cena: "<<m_puvodniCena<<std::endl;
    std::cout<<"Bourano: "<<m_bourano<<std::endl;
    std::cout<<"Cena: "<<OsobniAuto::getCena()<<std::endl;

}