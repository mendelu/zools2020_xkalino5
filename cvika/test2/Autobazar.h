//
// Created by kikak on 13.05.2020.
//

#ifndef TEST2_AUTOBAZAR_H
#define TEST2_AUTOBAZAR_H
#include <iostream>
#include "Automobil.h"
#include <vector>
class Autobazar{
       std::vector<Automobil*>m_auto;
public:
    Autobazar();
    void vypisAuta();
    int getMajetek();
    void pridejAutomobil(Automobil*automobil);
    int stocKm(int kolikStocit,Automobil*automobil);

};


#endif //TEST2_AUTOBAZAR_H
