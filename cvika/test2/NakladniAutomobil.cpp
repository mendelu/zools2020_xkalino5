//
// Created by kikak on 13.05.2020.
//

#include "NakladniAutomobil.h"
NakladniAutomobil::NakladniAutomobil(std::string model, int najetoKm, int puvodniCena, int nosnost)
:Automobil(model,najetoKm,puvodniCena){
    m_nosnost=nosnost;
}
int NakladniAutomobil::getCena() {
  return  (1 - (m_najetoKm/500000)) *m_puvodniCena;
}
void NakladniAutomobil::printInfo() {
    std::cout<<"Model: "<<m_modelAuta<<std::endl;
    std::cout<<"Najeto: "<<m_najetoKm<<std::endl;
    std::cout<<"Puvodni cena: "<<m_puvodniCena<<std::endl;
    std::cout<<"Nosnost: "<<m_nosnost<<std::endl;
    std::cout<<"Cena: "<<NakladniAutomobil::getCena()<<std::endl;
}