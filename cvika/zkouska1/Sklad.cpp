//
// Created by kikak on 17.05.2020.
//

#include "Sklad.h"
Sklad::Sklad(int pocetParkovachMist) {
    m_pocetParkovacichMist=pocetParkovachMist;
    for (Auto* &aut : m_auta){
     aut=nullptr;
    }
}
void Sklad::pridejAuto(Auto *aut) {
    if(m_auta.size()<m_pocetParkovacichMist) {
        m_auta.push_back(aut);
        std::cout << "Auto bylo pridano: "<<aut->getNazev()<<std::endl;
    } else {
        std::cout << "Parkoviste je plne.Auto "<<aut->getNazev()<<" nelze zaparkovat. "<<std::endl;
    }
}
void Sklad::printInfo() {
    std::cout<<"Stav parkoviste: "<< m_auta.size()<<std::endl;
    for(Auto* aut:m_auta ){
        if(aut!= nullptr){
            aut->printInfo();
        }
}}