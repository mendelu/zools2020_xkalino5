#include <iostream>
#include "Sklad.h"
#include "OsobniAuto.h"
#include "UzitkoveAuto.h"

int main() {
   Sklad* sklad=new Sklad(3);
OsobniAuto* auto1=new OsobniAuto("Ford",50,3,4,24);
OsobniAuto* auto2=new OsobniAuto("Fabia",40,5,3,20);
UzitkoveAuto* auto3=new UzitkoveAuto("Tatra",50,5,250,90);
UzitkoveAuto* auto5=new UzitkoveAuto("Lambo",40,5,250,90);
    sklad->pridejAuto(auto1);
    sklad->pridejAuto(auto2);
    sklad->pridejAuto(auto3);
    sklad->pridejAuto(auto5);
    sklad->printInfo();
    return 0;
}
