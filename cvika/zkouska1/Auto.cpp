//
// Created by kikak on 17.05.2020.
//

#include "Auto.h"
Auto::Auto(std::string nazev, int objem, int spotreba) {
    m_nazev=nazev;
    m_spotreba=spotreba;
    m_objemMotoru=objem;
}
std::string Auto::getNazev() {
    return m_nazev;
}
int Auto::getSpotreba() {
    return m_spotreba;
}
int Auto::getObjemMotoru() {
    return m_objemMotoru;
}