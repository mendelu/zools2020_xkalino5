//
// Created by kikak on 17.05.2020.
//

#ifndef ZKOUSKA1_OSOBNIAUTO_H
#define ZKOUSKA1_OSOBNIAUTO_H
#include <iostream>
#include "Auto.h"

class OsobniAuto: public Auto {
    int m_pocetSedadel;
    int m_objemUloznehoProstoru;
    int m_euronorma;
public:
    OsobniAuto(std::string nazev,int objem,int spotreba,int pocetSedadel,int objemProstoru);
    void printInfo();

};


#endif //ZKOUSKA1_OSOBNIAUTO_H
