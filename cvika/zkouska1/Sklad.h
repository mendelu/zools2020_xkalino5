//
// Created by kikak on 17.05.2020.
//

#ifndef ZKOUSKA1_SKLAD_H
#define ZKOUSKA1_SKLAD_H
#include <iostream>
#include <vector>
#include "Auto.h"
#include "OsobniAuto.h"
#include "UzitkoveAuto.h"
class Sklad {
    int m_pocetParkovacichMist;
    std::vector<Auto*>m_auta;
public:
    Sklad(int pocetParkovachMist);
    void pridejAuto(Auto*aut);
    void printInfo();

};


#endif //ZKOUSKA1_SKLAD_H
