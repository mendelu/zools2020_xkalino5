//
// Created by kikak on 17.05.2020.
//

#ifndef ZKOUSKA1_AUTO_H
#define ZKOUSKA1_AUTO_H

#include <iostream>

class Auto {
private:
    std::string m_nazev;
    int m_objemMotoru;
    int m_spotreba;
    Auto*aut;
public:
    Auto(std::string nazev,int objem,int spotreba);
    virtual void printInfo()=0;
    std::string getNazev();
    int getObjemMotoru();
    int getSpotreba();

};


#endif //ZKOUSKA1_AUTO_H
