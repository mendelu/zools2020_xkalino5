//
// Created by kikak on 17.05.2020.
//

#include "OsobniAuto.h"


 OsobniAuto::OsobniAuto(std::string nazev,int objem,int spotreba,int pocetSedadel, int objemProstoru):Auto(nazev,objem,spotreba)
 {
    m_pocetSedadel=pocetSedadel;
    m_objemUloznehoProstoru=objemProstoru;
    m_euronorma=((Auto::getObjemMotoru()*m_pocetSedadel)/Auto::getSpotreba());
 }
 void OsobniAuto::printInfo() {
     std::cout<<"Nazev: "<<Auto::getNazev()<<std::endl;
     std::cout<<"Objem: "<<Auto::getObjemMotoru()<<std::endl;
     std::cout<<"Spotreba: "<<Auto::getSpotreba()<<std::endl;
     std::cout<<"Pocet sedadel: "<<m_pocetSedadel<<std::endl;
     std::cout<<"Objem ulozneho prostoru: "<<m_objemUloznehoProstoru<<std::endl;
     std::cout<<"Euronorma: "<<m_euronorma<<std::endl;
}