//
// Created by kikak on 17.05.2020.
//

#include "UzitkoveAuto.h"
UzitkoveAuto::UzitkoveAuto(std::string nazev, int objem, int spotreba, int hmotnost, int objemNadrze):Auto(nazev,objem,spotreba)
{
    m_hmotnost=hmotnost;
    m_objemNadrze=objemNadrze;
    m_dojezd=(m_objemNadrze/Auto::getSpotreba());
}
void UzitkoveAuto::printInfo() {
    std::cout<<"Nazev: "<<Auto::getNazev()<<std::endl;
    std::cout<<"Objem: "<<Auto::getObjemMotoru()<<std::endl;
    std::cout<<"Spotreba: "<<Auto::getSpotreba()<<std::endl;
    std::cout<<"Hmotnost: "<<m_hmotnost<<std::endl;
    std::cout<<"Objem nadrze "<<m_objemNadrze<<std::endl;
    std::cout<<"Dojezd: "<<m_dojezd<<std::endl;
}