//
// Created by kikak on 17.05.2020.
//

#ifndef ZKOUSKA1_UZITKOVEAUTO_H
#define ZKOUSKA1_UZITKOVEAUTO_H
#include <iostream>
#include "Auto.h"

class UzitkoveAuto : public Auto{
    int m_hmotnost;
    int m_objemNadrze;
    int m_dojezd;
public:
    UzitkoveAuto(std::string nazev,int objem,int spotreba,int hmotnost,int objemNadrze);
    void printInfo();


};


#endif //ZKOUSKA1_UZITKOVEAUTO_H
